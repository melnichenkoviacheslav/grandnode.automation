﻿using TechTalk.SpecFlow;

namespace GrandNode.UI.Tests
{
    [Binding]
    public class StepsTransformations
    {
        [StepArgumentTransformation]
        public string ToString(string str)
            => str;

        [StepArgumentTransformation]
        public bool ToBoolean(string s)
        {
            switch (s)
            {
                case "checked":
                case "check":
                case "true":
                case "enabled":
                    return true;
                case "disabled":
                case "false":
                case "disappear":
                case "without":
                case "unchecked":
                case "uncheck":
                    return false;
                default:
                    return !s.ToUpper().Contains("NOT");
            }
        }

        //[StepArgumentTransformation]
        //public GrandNodePages ToDealWrapperPageType(string page)
        //    => page.GetValueFromDescription<GrandNodePages>();
    }
}