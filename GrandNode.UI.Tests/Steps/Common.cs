﻿using GrandNode.UI.Common.Contexts;
using TechTalk.SpecFlow;

namespace GrandNode.UI.Tests.Steps
{
    [Binding]
    public class Common
    {
        private CommonContext _commonContext;

        [Given(@"I open GranNode home page")]
        public void GivenIOpenGranNodeHomePage()
        {
            _commonContext = new CommonContext();
        }

        [When(@"I enter '(.*)' in top search field")]
        public void WhenIEnterInTopSearchField(string value)
        {
            _commonContext.FillTopSearch(value);
        }

        [When(@"I submit top search request")]
        public void WhenISubmitTopSearchRequest()
        {
            _commonContext.SubmitTopSearch();
        }

        [Given(@"I select '(.*)' subproduct from '(.*)' product menu")]
        public void GivenISelectSubproductFromProductMenu(string subProduct, string product)
        {
            _commonContext.SelectItemFromProductMenu(product, subProduct);
        }
    }
}