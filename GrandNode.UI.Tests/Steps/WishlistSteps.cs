﻿using FluentAssertions;
using GrandNode.UI.Common.Contexts;
using TechTalk.SpecFlow;

namespace GrandNode.UI.Tests.Steps
{
    [Binding]
    public class WishlistSteps
    {
        public ScenarioContext ScenarioContext { get; }
        private WishlistContext WishlistContext;

        public WishlistSteps(ScenarioContext context)
        {
            ScenarioContext = context;
            WishlistContext = new WishlistContext();
        }

        [Then(@"I see wished product at wishlist page")]
        public void ThenISeeWishedProductAtWishlistPage()
        {
            var actualProductName = WishlistContext.GetProductName();
            ScenarioContext.TryGetValue("expectedName", out string expectedName);
            actualProductName.Should().Contain(expectedName);
        }

    }
}
