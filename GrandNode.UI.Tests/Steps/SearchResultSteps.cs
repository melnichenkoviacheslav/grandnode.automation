﻿using FluentAssertions;
using FluentAssertions.Execution;
using GrandNode.UI.Common.Contexts;
using TechTalk.SpecFlow;

namespace GrandNode.UI.Tests.Steps
{
    [Binding]
    public class SearchResultSteps
    {
        public ScenarioContext ScenarioContext { get; }
        private SearchResultContext SearchResultContext;

        public SearchResultSteps(ScenarioContext context)
        {
            ScenarioContext = context;
            SearchResultContext = new SearchResultContext();
        }

        [Then(@"All search results contains '(.*)' text")]
        public void ThenAllSearchResultsContainsText(string searchIdentifier)
        {
            var actualProductsNames = SearchResultContext.GetProductNames();

            using (new AssertionScope())
            {
                foreach (var name in actualProductsNames)
                {
                    name.ToLower().Should().Contain(searchIdentifier.ToLower());
                }
            }
        }

        [Given(@"I select product by name '(.*)'")]
        public void GivenISelectProductByName(string expectedName)
        {
            SearchResultContext.SelectProductFromCategory(expectedName);
            ScenarioContext.Add("expectedName", expectedName);
        }

        [When(@"I switch sorting to '(.*)' at search result page")]
        public void WhenISwitchSortingToAtSearchResultPage(string sortingOption)
        {
            SearchResultContext.ChangeSortingOption(sortingOption);
        }

        [Then(@"I see product list sorted from A to Z at search result page")]
        public void ThenISeeProductListSortedFromAToZAtSearchResultPage()
        {
            var productsNames = SearchResultContext.GetProductNames();
            productsNames.Should().BeInAscendingOrder();
        }

        [Then(@"I see product list sorted from Z to A at search result page")]
        public void ThenISeeProductListSortedFromZToAAtSearchResultPage()
        {
            var productsNames = SearchResultContext.GetProductNames();
            productsNames.Should().BeInDescendingOrder();
        }

        [Then(@"I see product list sorted by price from Low to High at search result page")]
        public void ThenISeeProductListSortedByPriceFromLowToHighAtSearchResultPage()
        {
            var productsNames = SearchResultContext.GetProductPrices();
            productsNames.Should().BeInAscendingOrder();
        }

        [Then(@"I see product list sorted by price from High to Low at search result page")]
        public void ThenISeeProductListSortedByPriceFromHighToLowAtSearchResultPage()
        {
            var productsNames = SearchResultContext.GetProductPrices();
            productsNames.Should().BeInDescendingOrder();
        }
    }
}
