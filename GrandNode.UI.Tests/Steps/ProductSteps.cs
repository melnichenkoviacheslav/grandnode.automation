﻿using GrandNode.UI.Common.Contexts;
using TechTalk.SpecFlow;

namespace GrandNode.UI.Tests.Steps
{
    [Binding]
    public class ProductSteps
    {

        private ProductContext _productContext;

        public ProductSteps()
        {
            _productContext = new ProductContext();
        }

        [When(@"I add current product to wishlist at product page")]
        public void WhenIAddCurrentProductToWishlistAtProductPage()
        {
            _productContext.AddToWishList();
            _productContext.ProcessModalWindow();
        }
    }
}
