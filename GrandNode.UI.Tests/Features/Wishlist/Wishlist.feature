﻿Feature: Wishlist

Scenario Outline: Should add item to the wishlist
	Given I open GranNode home page
	And I select 'Notebooks' subproduct from 'Computers' product menu
	And I select product by name '<Name>'
	When I add current product to wishlist at product page
	Then I see wished product at wishlist page

	Examples:
		| Name    |
		| Apple   |
		| Asus    |
		| Samsung |