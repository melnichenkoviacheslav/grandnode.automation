﻿Feature: Sorting

Scenario: Should perform sorting from A to Z
	Given I open GranNode home page
	And I select 'Notebooks' subproduct from 'Computers' product menu
	When I switch sorting to 'Name: A to Z' at search result page
	Then I see product list sorted from A to Z at search result page

Scenario: Should perform sorting from Z to A
	Given I open GranNode home page
	And I select 'Notebooks' subproduct from 'Computers' product menu
	When I switch sorting to 'Name: Z to A' at search result page
	Then I see product list sorted from Z to A at search result page

Scenario: Should perform sorting by price: Low to High
	Given I open GranNode home page
	And I select 'Notebooks' subproduct from 'Computers' product menu
	When I switch sorting to 'Price: Low to High' at search result page
	Then I see product list sorted by price from Low to High at search result page

Scenario: Should perform sorting by price: High to Low
	Given I open GranNode home page
	And I select 'Notebooks' subproduct from 'Computers' product menu
	When I switch sorting to 'Price: High to Low' at search result page
	Then I see product list sorted by price from High to Low at search result page
