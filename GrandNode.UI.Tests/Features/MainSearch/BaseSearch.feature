﻿Feature: BaseSearch

Scenario Outline: Should search specified items by text
	Given I open GranNode home page
	When I enter '<Text>' in top search field
	And I submit top search request
	Then All search results contains '<Text>' text

	Examples:
		| Text  |
		| nikon |
		| apple |
		| htc	|