using System;
using System.Collections.Generic;
using System.Reflection;
using Atata;
using GrandNode.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Opera;
using TechTalk.SpecFlow;
using LogLevel = Atata.LogLevel;

namespace GrandNode.UI.Tests
{
    [Binding]
    public class SpecflowHooks
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks
        private AtataContextBuilder _atataContextBuilder;

        private Browser _browser;

        [BeforeScenario]
        public void BeforeFeature()
        {
            InitContext();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            AtataContext.Current.Driver.Manage().Cookies.DeleteAllCookies();
            AtataContext.Current?.CleanUp();
        }

        private void InitContext()
        {
            _browser = ConfigurationHelper.GetBrowser();
            _atataContextBuilder = AtataContext.Configure();
            SetupDriver();
            _atataContextBuilder.Build();
            AtataContext.Current.Driver.Maximize();
        }

        private void SetupDriver()
        {
            if (ConfigurationHelper.IsTeamCityRun)
            {
                SetupRemoteDriver();
            }
            else
            {
                SetUpLocalDriver();
            }

            _atataContextBuilder
                .UseBaseUrl(ConfigurationHelper.MainUrl)
                .UseElementFindTimeout(ConfigurationHelper.ElementTimeOut)
                .UseWaitingRetryInterval(ConfigurationHelper.RetryTimeOut)
                .UseCulture("en-us")
                .AddNUnitTestContextLogging()
                .WithMinLevel(LogLevel.Debug)
                .UseNUnitTestName()
                .TakeScreenshotOnNUnitError()
                .AddScreenshotFileSaving()
                .WithFolderPath(() =>
                    $@"ScreenShots\{ConfigurationHelper.ScreenshotsFolderPath}{AtataContext.BuildStart:yyyy-MM-dd-HH-mm-ss}")
                .WithFileName(screenshotInfo => $"{screenshotInfo.PageObjectName}_{AtataContext.Current.TestName}");
        }

        private void SetUpLocalDriver()
        {
            switch (_browser)
            {
                case Browser.Chrome:
                    _atataContextBuilder
                        .UseChrome()
                        .WithFixOfCommandExecutionDelay()
                        .WithArguments("disable-infobars", $"--homepage=${ConfigurationHelper.MainUrl}",
                            "disable-extensions")
                        .WithOptions(x => x.AddUserProfilePreference("credentials_enable_service", false));
                    break;
                case Browser.Firefox:
                    _atataContextBuilder
                        .UseFirefox()
                        .WithFixOfCommandExecutionDelay();
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"No such browser {_browser}!");
            }
        }

        private void SetupRemoteDriver()
        {
            var version = ConfigurationHelper.GetBrowserVersion();
            DriverOptions options = null;
            switch (_browser)
            {
                case Browser.Chrome:
                    options = new ChromeOptions();
                    break;
                case Browser.Firefox:
                    options = new FirefoxOptions();
                    break;
                case Browser.Opera:
                    options = new OperaOptions();
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"No such browser {_browser}!");
            }

            options.BrowserVersion = version;
            options.AcceptInsecureCertificates = true;

            var additionalCaps = new Dictionary<string, object>
            {
                {RemoteWebDriverCapability.EnableVnc, ConfigurationHelper.Vnc},
                {RemoteWebDriverCapability.EnableVideo, ConfigurationHelper.EnableVideoRecording},
                {RemoteWebDriverCapability.ScreenResolution, ConfigurationHelper.ScreenResolution}
            };

            //Set additional capabilities using reflection due to DriverOptions limitations
            options.GetType()
                .GetField("additionalCapabilities", BindingFlags.NonPublic | BindingFlags.Instance)?
                .SetValue(options, additionalCaps);

            _atataContextBuilder
                .UseRemoteDriver()
                .WithCapabilities(options.ToCapabilities())
                .WithRemoteAddress(ConfigurationHelper.SelenoidHub);
        }
    }
}