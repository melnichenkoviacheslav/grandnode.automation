﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace GrandNode.Helpers
{
    public static class ConfigurationHelper
    {
        public static TimeSpan ElementTimeOut
        {
            get
            {
                var elementTimeOut = Environment.GetEnvironmentVariable("elements.elementTimeOut");
                double.TryParse(elementTimeOut ?? GetLocalSettings("Elements", "elementTimeOut"), out var result);
                return TimeSpan.FromMilliseconds(result);
            }
        }

        public static bool EnableVideoRecording
            => bool.Parse(Environment.GetEnvironmentVariable("browser.enableVideoRecording") ?? GetLocalSettings("Browser", "enableVideoRecording"));

        public static bool IsTeamCityRun
            => !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("TEAMCITY_VERSION"));

        public static string MainUrl
            => Environment.GetEnvironmentVariable("grandNodeEndpoint")
               ?? GetLocalSettings("App", "homeUrl");

        public static TimeSpan RetryTimeOut
        {
            get
            {
                var retryTimeOut = Environment.GetEnvironmentVariable("elements.retryTimeOut");
                double.TryParse(retryTimeOut ?? GetLocalSettings("Elements", "retryTimeOut"), out var result);
                return TimeSpan.FromMilliseconds(result);
            }
        }

        public static string ScreenResolution
            => Environment.GetEnvironmentVariable("browser.screenResolution")
               ?? GetLocalSettings("Browser", "screenResolution");

        public static string ScreenshotsFolderPath
            => Environment.GetEnvironmentVariable("browser.screenshotsFolderPath")
               ?? GetLocalSettings("Browser", "screenshotsFolderPath");

        public static string SelenoidHub
            => Environment.GetEnvironmentVariable("selenoid.hub")
               ?? GetLocalSettings("Selenoid", "hub");

        public static bool Vnc
            => bool.Parse(Environment.GetEnvironmentVariable("browser.vnc") ?? GetLocalSettings("Browser", "vnc"));

        public static Browser GetBrowser()
        {
            var browserSource = IsTeamCityRun
                ? Environment.GetEnvironmentVariable("browser.name")
                : GetLocalSettings("Browser", "name");

            if (Enum.TryParse<Browser>(browserSource, ignoreCase: true, out var browser))
            {
                return browser;
            }

            throw new ArgumentOutOfRangeException($"'{browser}' is unexpected browser!");
        }

        public static string GetBrowserVersion()
        {
            var browserVersion = IsTeamCityRun
                ? Environment.GetEnvironmentVariable("browser.version")
                : GetLocalSettings("Browser", "version");

            if (string.IsNullOrEmpty(browserVersion))
            {
                throw new ArgumentOutOfRangeException($"'{browserVersion}' is unexpected browser version!");
            }

            return browserVersion;
        }

        private static string GetLocalSettings(string sectionName, string settingName)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false)
                .AddEnvironmentVariables()
                .Build();

            var setting = configuration.GetSection(sectionName);
            if (setting == null)
            {
                throw new Exception($"'{settingName}' setting is missing!");
            }

            return setting[settingName];
        }
    }
}