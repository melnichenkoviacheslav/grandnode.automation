﻿namespace GrandNode.Helpers
{
    public static class RemoteWebDriverCapability
    {
        public const string EnableVnc = "enableVNC";
        public const string EnableVideo = "enableVideo";
        public const string VideoFileName = "videoFileName";
        public const string ScreenResolution = "screenResolution";
    }
}
