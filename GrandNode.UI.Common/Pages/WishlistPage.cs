﻿using Atata;

namespace GrandNode.UI.Common.Pages
{
    using _ = WishlistPage;
    
    public class WishlistPage : BasePage<_>
    {
        [FindByClass("product-name")]
        public Link<_> ProductName { get; set;}
    }

}