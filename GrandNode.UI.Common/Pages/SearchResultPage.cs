﻿using Atata;

namespace GrandNode.UI.Common.Pages
{
    using _ = SearchResultPage;

    public class SearchResultPage : BasePage<_>
    {
        [FindByClass("product-grid")]
        public ControlList<ProductControl<_>, _> FoundProducts { get; set; }

        [FindById("products-orderby")]

        public Select<_> SortBy { get; set; }
    }

    [ControlDefinition("div", ContainingClass = "product-box")]
    public class ProductControl<T> : Control<T> where T : PageObject<T>
    {
        public ProductInfoLabel<T> ProductInfoLabel { get; set; }
    }

    [ControlDefinition("div", ContainingClass = "product-info")]
    public class ProductInfoLabel<T> : Control<T> where T : PageObject<T>
    {
        [FindByCss(".title a")]
        public Link<T> ProductNameLink { get; set; }

        [FindByClass("actual-price price")]
        public Text<T> ProductActualPrice { get; set; }
    }
}