﻿using Atata;

namespace GrandNode.UI.Common.Pages
{
    using _ = ProductItemPage;

    public class ProductItemPage : BasePage<_>
    {
        [FindByClass("qty-input")]
        public TextInput<_> Quantity { get; set; }

        [FindByClass("add-to-cart-button")]
        public Button<_> AddToCart { get; set; }

        [FindByClass("add-to-wishlist-button")]
        public Button<_> AddToWishlistButton { get; set; }

        [FindByClass("add-to-compare-list-button")]
        public Button<_> AddToCompare { get; set; }

        [FindByClass("email-a-friend-button")]
        public Button<_> EmailFriend { get; set; }
    }
}
