﻿using Atata;
using GrandNode.UI.Common.Pages.Controls;

namespace GrandNode.UI.Common.Pages
{
    public class BasePage<T> : Page<T> where T : Page<T>
    {
        [FindByClass("headBottom")]
        public Header<T> Header { get; set; }

        [FindByClass("mainNav")]
        public NavigationMenu<T> NavigationMenu { get; set; }

        [FindByClass("search-input")]
        public TextInput<T> SearchInput { get; set; }

        [ControlDefinition("div[@id='ModalAddToCart']")]
        public ModalWindowControl<T> ModalWindow { get; set; }
    }
}