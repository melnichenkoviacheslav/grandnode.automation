﻿using Atata;

namespace GrandNode.UI.Common.Pages.Controls
{
    public partial class Header<T> : Control<T> where T : Page<T>
    {
        [ControlDefinition("input[@type='search' or not(@type)]")]
        [FindByClass("search-box-text")]
        public Input<string, T> SearchFieldInput { get; set; }

        [FindByClass("search-box-button")]
        public Button<T> SubmitButton { get; set; }

        [FindByClass("user-panel-trigger")]
        public Link<T> UserProfileLink { get; set; }

        [FindByCss(".wishlist-container a")]
        public Link<T> WishListLink { get; set; }

        [FindByCss(".cart-container a")]
        public Link<T> CartContainerLink { get; set; }
    }
}
