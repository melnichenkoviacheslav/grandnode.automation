﻿using Atata;

namespace GrandNode.UI.Common.Pages.Controls
{
    public class ModalWindowControl<T> : Control<T> where T : PageObject<T>
    {
        [FindByClass("btn-outline-info")]
        public Button<T> ContinueButton { get; set; }

        [FindByClass("btn-info")]
        public Button<T> ProcessButton { get; set; }

        [FindByClass("modal-header")]
        public H5<T> Header { get; set; }
    }
}