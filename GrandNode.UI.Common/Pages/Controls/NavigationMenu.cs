﻿using Atata;

namespace GrandNode.UI.Common.Pages.Controls
{
    public class NavigationMenu<T> : Control<T> where T : Page<T>
    {
        public ControlList<NavigationMenuItem<T>, T> NavigationMenuItems { get; set; }
    }
    
    [ControlDefinition(".//ul[@class='navbar-nav']/li")]
    public class NavigationMenuItem<T> : Control<T> where T : PageObject<T>
    {
        [FindByCss("a>span")]
        public Text<T> MenuItemName { get; set; }

        public ControlList<NavigationMenuSubItem<T>, T> SubItems { get; set; }
    }

    [ControlDefinition("li", ContainingClass = "nav-item")]
    public class NavigationMenuSubItem<T> : Control<T> where T : PageObject<T>
    {
        [FindByCss(".nav-link span")]
        public Text<T> SubItemName { get; set; }

        [FindByClass("nav-link")]
        public Link<T> SubItemLink { get; set; }
    }
}