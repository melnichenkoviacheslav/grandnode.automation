﻿using Atata;
using GrandNode.UI.Common.Pages;

namespace GrandNode.UI.Common.Contexts
{
    public class WishlistContext
    {
        protected readonly WishlistPage WishlistPage;

        public WishlistContext()
        {
            WishlistPage = Go.To<WishlistPage>();
        }

        public string GetProductName() => WishlistPage.ProductName.Content.Value;
    }
}