﻿using System.Collections.Generic;
using System.Linq;
using Atata;
using GenericExtensions.src;
using GrandNode.UI.Common.Pages;

namespace GrandNode.UI.Common.Contexts
{
    public class SearchResultContext
    {
        protected readonly SearchResultPage SearchResultPage;

        public SearchResultContext()
        {
            SearchResultPage = Go.To<SearchResultPage>();
        }

        public List<string> GetProductNames()
        {
            return SearchResultPage
                .FoundProducts
                .Select(p => p.ProductInfoLabel.ProductNameLink.Content.Value).ToList();
        }

        public List<string> GetProductPrices()
        {
            return SearchResultPage
                .FoundProducts
                .Select(p => p.ProductInfoLabel.ProductActualPrice.Content.Value).ToList();
        }

        public void SelectProductFromCategory(string productName)
        {
            var product =
            SearchResultPage.FoundProducts
                .First(p => p.ProductInfoLabel.ProductNameLink.Content.Value.Contains(productName));

            product.Hover();
            product.Click();
        }

        public void ChangeSortingOption(string sortingOption)
        {
            Wait.For(() => SearchResultPage.SortBy.IsEnabled);
            SearchResultPage.SortBy.Set(sortingOption);
        }
    }
}