﻿using Atata;
using GenericExtensions.src;
using GrandNode.UI.Common.Pages;
using System.Diagnostics;
using System.Threading;

namespace GrandNode.UI.Common.Contexts
{
    public class ProductContext : CommonContext
    {
        protected ProductItemPage ProductItemPage;

        public ProductContext()
        {
            ProductItemPage = Go.To<ProductItemPage>();
        }

        public void AddToWishList()
        {
            Wait.For(() => ProductItemPage.AddToWishlistButton.IsEnabled);
            ProductItemPage.AddToWishlistButton.Hover();
            ProductItemPage.AddToWishlistButton.Click();
        }
    }
}