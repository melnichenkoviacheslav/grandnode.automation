﻿using System.Linq;
using Atata;
using GenericExtensions.src;
using GrandNode.UI.Common.Pages;

namespace GrandNode.UI.Common.Contexts
{
    public class CommonContext
    {
        public CommonContext()
        {
            BasePage = Go.To<HomePage>();
        }

        protected HomePage BasePage { get; }

        public void FillTopSearch(string value)
        {
            BasePage.Header.SearchFieldInput.Set(value);
        }

        public void SubmitTopSearch()
        {
            BasePage.Header.SubmitButton.Click();
        }

        public void SelectItemFromProductMenu(string productName, string subProductName)
        {
            var menuItem = BasePage.NavigationMenu.NavigationMenuItems.Single(item => item.MenuItemName.Content.Value.Equals(productName));
            menuItem.Hover();
            menuItem.SubItems.Single(item => item.SubItemName.Get().Equals(subProductName)).SubItemLink.Click();
        }

        public void ProcessModalWindow()
        {
            Wait.For(() => BasePage.ModalWindow.Header.Get().Contains("Product was successfully added to wishlist"));
            BasePage.ModalWindow.ProcessButton.Click();
        }
    }
}
